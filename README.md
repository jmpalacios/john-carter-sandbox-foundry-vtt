# John Carter de Marte

<a href='https://ko-fi.com/fantasticmrpanda' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=2' border='0' alt='¡Apóyame en Ko-Fi!' /></a>

 
Ficha y sistema NO OFICIAL de **John Carter de Marte** para Foundry VTT usando [Sandbox](https://gitlab.com/rolnl/sandbox-system-builder/) de [Seregras](https://www.youtube.com/c/RolNL/).

**Requisitos mínimos:** Sandbox System Builder versión 0.3.9 o mayor. 
**Compatibilidad:** Core version 0.6.6.

**AVISO** La versión actual de Sandbox System Builder tiene conocidos problemas de compatibilidad con las versiones 0.7.0 o superiores de Foundry VTT, por lo que este world no funciona adecuadamente en dichas versiones. Recomiendo seguir el log de cambios de [Sandbox](https://gitlab.com/rolnl/sandbox-system-builder/) para más información.

Considera apoyar el [Patreon](https://www.patreon.com/seregras) de Seregras autor de Sandbox y contribuye a mejoras y ampliaciones del sistema.

Instalación recomendada usando este enlace [world.json](https://gitlab.com/jmpalacios/john-carter-sandbox-foundry-vtt/-/raw/master/world.json) desde **Game Worlds** y poniendo el enlace en *Manifest Url*.

Espero que lo disfrutéis.
